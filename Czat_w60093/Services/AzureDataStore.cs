﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Czat_w60093.Models;
using Czat_w60093.ViewModels;
using System.Diagnostics;
using System.Runtime.InteropServices.ComTypes;
using Czat_w60093.Views;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Czat_w60093.Services
{
    public class AzureDataStore : IDataStore<Item>
    {
        HttpClient client;
        IEnumerable<Item> items;

        public AzureDataStore()
        {
#if DEBUG
            var insecureHandler = GetInsecureHandler();
            client = new HttpClient(insecureHandler);
#else
            client = new HttpClient();

#endif
            client.BaseAddress = new Uri($"{App.AzureBackendUrl}/");

            items = new List<Item>();
        }

        bool IsConnected => Connectivity.NetworkAccess == NetworkAccess.Internet;
        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            if (forceRefresh && IsConnected)
            {
                var json = await client.GetStringAsync($"api/item");
                items = await Task.Run(() => JsonConvert.DeserializeObject<IEnumerable<Item>>(json));
            }

            return items;
        }

        public async Task<Item> GetItemAsync(string id)
        {
            if (id != null && IsConnected)
            {
                var json = await client.GetStringAsync($"api/item/{id}");
                return await Task.Run(() => JsonConvert.DeserializeObject<Item>(json));
            }

            return null;
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            if (item == null || !IsConnected)
                return false;

            var serializedItem = JsonConvert.SerializeObject(item);

            var response = await client.PostAsync($"api/item", new StringContent(serializedItem, Encoding.UTF8, "application/json"));

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            if (item == null || item.Id == null || !IsConnected)
                return false;

            var serializedItem = JsonConvert.SerializeObject(item);
            var buffer = Encoding.UTF8.GetBytes(serializedItem);
            var byteContent = new ByteArrayContent(buffer);

            var response = await client.PutAsync(new Uri($"api/item/{item.Id}"), byteContent);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            if (string.IsNullOrEmpty(id) && !IsConnected)
                return false;

            var response = await client.DeleteAsync($"api/item/{id}");

           

            return response.IsSuccessStatusCode;
        }




        public HttpClientHandler GetInsecureHandler()
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (message, cert, chain,
           errors) =>
            {
                return true;
            };
            return handler;
        }

        //public async Task<boo> LoginUser(string emeil, string password)
        //{
        //    var keyValues = new List<KeyValuePair<string, string>>
        //    {
        //        new KeyValuePair<string, string>("emeil", emeil),
        //        new KeyValuePair<string, string>("pass", password)
        //    };
        //    var request = new HttpRequestMessage(HttpMethod.Post, $"api/logowanie");
        //    request.Content = new FormUrlEncodedContent(keyValues);
        //    var response = await client.SendAsync(request);

        //    return response.IsSuccessStatusCode;
        //}


        //public async Task<bool> LoginUser(string emeil, string password)
        //{


        //    if (emeil == null || !IsConnected)
        //        return false;
        //    else if (password == null || !IsConnected)
        //        return false;

        //    var serializeEmail= JsonConvert.SerializeObject(emeil);
        //    var serializedPass = JsonConvert.SerializeObject(password);

        //    var response = await client.PostAsync($"api/item", new StringContent(serializeEmail, Encoding.UTF8, "application/json"));
        //    var response_2 = await client.PostAsync($"api/item", new StringContent(serializedPass, Encoding.UTF8, "application/json"));

        //    return (response.IsSuccessStatusCode);


        //}
        public async Task<Item> LoginUser(LoginViewModel log)
        {

            if (log == null || !IsConnected)
                return null;

            var serializeLgow = JsonConvert.SerializeObject(log);

            var response = await client.PostAsync($"api/logowanie", new StringContent(serializeLgow, Encoding.UTF8, "application/json"));



            return await Task.Run(() => JsonConvert.DeserializeObject<Item>(response.Content.ReadAsStringAsync().Result));

        }

        //public async Task<Item> GetItemsAsync()
        //{
        //    var response = await client.GetAsync($"api/logowanie");
        //    if (response.IsSuccessStatusCode)
        //    {
        //        var json = await response.Content.ReadAsStringAsync();
        //        return JsonConvert.DeserializeObject<Item>(json);

        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}


        public Task<bool> LoginUser(string emeil, string password)
        {
            throw new NotImplementedException();
        }
    }
}
