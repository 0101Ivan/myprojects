﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Czat_w60093.Models;

namespace Czat_w60093.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>()
            {
               new Item { Id = Guid.NewGuid().ToString(), Name = "Ivan1", Surname ="Melchenko1", Nr_albomu = 60093 ,plec="M" },
                new Item { Id = Guid.NewGuid().ToString(),  Name = "Ivan2",Surname="Melchenko2" ,Nr_albomu = 60093 ,plec="M" },
                new Item { Id = Guid.NewGuid().ToString(),  Name = "Ivan3", Surname="Melchenko3",Nr_albomu = 60093 , plec="M" },
                new Item { Id = Guid.NewGuid().ToString(),  Name = "Ivan4", Surname="Melchenko4", Nr_albomu = 60093 ,plec="M" },
                new Item { Id = Guid.NewGuid().ToString(),  Name = "Ivan5", Surname="Melchenko5", Nr_albomu = 60093 ,plec="M" },
                new Item { Id = Guid.NewGuid().ToString(),  Name = "Ivan6", Surname="Melchenko6",Nr_albomu = 60093 , plec="M"  }
            };
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }

        public Task<bool> LoginUser(string emeil, string password)
        {
            throw new NotImplementedException();
        }
    }
}