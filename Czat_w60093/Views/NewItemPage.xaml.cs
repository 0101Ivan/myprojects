﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Czat_w60093.Models;

namespace Czat_w60093.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class NewItemPage : ContentPage
    {
        public Item Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();

            Item = new Item
            {

                Name = "Ivan ",
                Surname = "Melchenko ",
                Nr_albomu = 600,
                plec = " M",
                emeil = "Ivan.mel ",
                pass = "123 ",
                imgUrl = "Ivan"

            };

            BindingContext = this;
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            var page = new ItemsPage();
            MessagingCenter.Send(this, "AddItem", Item);
            await Navigation.PushAsync(page);
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}