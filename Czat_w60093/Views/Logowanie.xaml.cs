﻿using Czat_w60093.Services;
using Czat_w60093.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace Czat_w60093.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Logowanie : ContentPage
    {
        private readonly AzureDataStore azureData = new AzureDataStore();
        private LoginViewModel lok;
        public Logowanie()
        {

            
            lok = new LoginViewModel
            {
                emeil = "",
                password = ""
            };
            lok.Navi = this.Navigation;

            BindingContext = lok;
            InitializeComponent();
        }

        private async void ZAL_BUT(object sender, EventArgs e)
        {

           
            if (StaClaLog.user != null) {
                await Navigation.PushAsync(new ItemsPage());
            }
            

        }





        private async void Nie_mam_kont(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewItemPage());

        }
    }

}