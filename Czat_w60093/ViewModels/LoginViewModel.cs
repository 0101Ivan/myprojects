﻿using Czat_w60093.Models;
using Czat_w60093.Services;
using Czat_w60093.Views;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Czat_w60093.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private AzureDataStore _azureAPI = new AzureDataStore();

        
        public string emeil { get; set; }
        public string password { get; set; }
        public bool success { get; set; }
        public Item zaluse { get; set; }
        public INavigation Navi;
       

    public LoginViewModel()
        {
           

        }

        public LoginViewModel(string emeil, string password)
        {
            this.emeil = emeil;
            this.password = password;
        }

        public ICommand LoginCommand
        {
            get
            {
                return new Command(async () =>
                {
                    LoginViewModel log = new LoginViewModel(emeil, password);
                    zaluse = await _azureAPI.LoginUser(log);
                    StaClaLog.user = zaluse;

                });
            }
        }
        //public async Task<bool> Login()
        //{
           
          
        //   return zaluse != null;

        //}

    }
}
