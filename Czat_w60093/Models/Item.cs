﻿using System;

namespace Czat_w60093.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Nr_albomu { get; set; }
        public string plec { get; set; }
        public string emeil { get; set; }
        public string pass { get; set; }
        public string imgUrl { get; set; }
    }
}