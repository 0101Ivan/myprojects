﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace Czat_w60093.Models
{
    public class ItemRepository : IItemRepository
    {
        private static ConcurrentDictionary<string, Item> items =
            new ConcurrentDictionary<string, Item>();

        public ItemRepository()
        {
            if (items.Count == 0)
            {
                Add(new Item { Id = Guid.NewGuid().ToString(), name = "Ivan", Surname = "Melchenko", Nr_albomu = 60093, plec = "M", emeil = "ivan.melchenko.01@mail.ru", pass = "123zxc123", imgUrl = "Ivan.jpg" });
                Add(new Item { Id = Guid.NewGuid().ToString(), name = "Vlad", Surname = "Petruk", Nr_albomu = 60083, plec = "M", emeil = "vlad.petruk.01@mail.ru", pass = "123zxc123", imgUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Papio_anubis_%28Serengeti%2C_2009%29.jpg/200px-Papio_anubis_%28Serengeti%2C_2009%29.jpg" });
            }
        }

        public IEnumerable<Item> GetAll()
        {
            return items.Values;
        }

        public void Add(Item item)
        {
            item.Id = Guid.NewGuid().ToString();
            items[item.Id] = item;
        }

        public Item Get(string id)
        {
            items.TryGetValue(id, out Item item);
            return item;
        }

        public Item Remove(string id)
        {
            items.TryRemove(id, out Item item);
            return item;
        }

        public void Update(Item item)
        {
            items[item.Id] = item;
        }

       
    }
}
