﻿using Czat_w60093.Models;
using Czat_w60093.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Czat_w60093.Web.Models
{
    public class DbItemRepository : IItemRepository
    {
        private readonly MyDbContext _myDbContext;

        public DbItemRepository(MyDbContext myDbContext)
        {
            _myDbContext = myDbContext;
        }
        public void Add(Item item)
        {

            _myDbContext.Items.Add(item);
            _myDbContext.SaveChanges();
        }
        public Item Get(string id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Item> GetAll()
        {
            return _myDbContext.Items.ToList();
        }

        public Item Remove(string key)
        {
            throw new NotImplementedException();
        }

        public void Update(Item item)
        {
            _myDbContext.Items.Add(item);
            _myDbContext.SaveChanges();
        }
        public void Post()
        {


        }


    }
}
