﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Czat_w60093.Models
{
    public class Item
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }


        [Required]
        public string name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public int Nr_albomu { get; set; }
        [Required]
        public string plec { get; set; }
        [Required]
        public string emeil { get; set; }
        [Required]
        public string pass { get; set; }
        [Required]
        public string imgUrl { get; set; }
    }
}
