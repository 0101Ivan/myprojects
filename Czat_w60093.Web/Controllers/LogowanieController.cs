﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Czat_w60093.Models;
using Czat_w60093.ViewModels;
using Czat_w60093.Web.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Czat_w60093.Web.Controllers
{

    [Route("api/logowanie")]
    [ApiController]
    public class LogowanieController : ControllerBase
    {
        MyDbContext _myDbContext = new MyDbContext();
        private readonly IItemRepository ItemRepository;
        private Item item;
        // POST api/<controller>
        public LogowanieController(IItemRepository itemRepository)
        {
            ItemRepository = itemRepository;
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Item> Post([FromBody]LoginViewModel log)
        {
            if (_myDbContext.Items.Any(user => user.emeil.Equals(log.emeil)))
            {
                var user = _myDbContext.Items.Where(u => u.emeil.Equals(log.emeil)).First();

                if (log.emeil.Equals(user.emeil))
                {
                    if (log.password.Equals(user.pass))
                    {
                        this.item = user;
                        return item;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }
        

    }
}
        
