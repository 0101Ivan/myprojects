﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Czat_w60093.Web.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    Surname = table.Column<string>(nullable: false),
                    Nr_albomu = table.Column<int>(nullable: false),
                    plec = table.Column<string>(nullable: false),
                    emeil = table.Column<string>(nullable: false),
                    pass = table.Column<string>(nullable: false),
                    imgUrl = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Items");
        }
    }
}
