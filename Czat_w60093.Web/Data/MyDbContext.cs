﻿using Czat_w60093.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Czat_w60093.Web.Data
{
    public class MyDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\MSSQLSERPROJECTS; Database=Czat_w60093; Trusted_Connection=True;");
        }


        public DbSet<Item> Items { get; set; }


    }
}
